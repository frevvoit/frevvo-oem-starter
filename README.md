# frevvo OEM Starter #

This repository contains starter files for customizing a frevvo OEM installation.

# How It Works #

Here we assume that frevvo was installed in the `/frevvo/` folder where `frevvo.war` is located in `/frevvo/tomcat/webapps/frevvo.war`.

## Download the starter files
First unzip the `/frevvo/tomcat/webapps/frevvo.war` to `/frevvo/tomcat/webapps/frevvo/`.

The [download](https://bitbucket.org/frevvoit/frevvo-oem-starter/downloads/) the frevvo-oem-starter zip file and unzip it to the frevvo installation, i.e. the `/frevvo/` folder. The zip file layout should match exactly the frevvo install layout.

The frevvo-oem-starter provides a custom `/frevvo/tomcat/conf/frevvo-config.properties` settings that can be used to override branding and related OEM properties such as customizing designer toolbar buttons etc.

## Enable the automatic OEM tenant creation

frevvo provides a way to customize the automatic database schema creation by allowing OEMs to provide their own Liquibase `db_changelog.xml` file. And the frevvo-oem-started provides a custom `db_changelog.xml` that was saved to `/frevvo/tomcat/webapps/frevvo/WEB-INF/classes/com/frevvo/sql/oem/` when it was unzipped. The same `db_changelog.xml` file will automatically create a tenant named `OEM` with one tenant admin user named `admin`. As long as the `db_changelog.xml` is located in `/WEB-INF/classes/com/frevvo/sql/oem/` it will be picked up automatically by frevvo.


## What now?

Now that you have these files being used by `frevvo.war`, what to do now?

There are three main areas that will need customizations based on your integration project:

## Branding

It is likely you will need to further customize branding in `${catalina.base}/frevvo-config.properties`. More information on the different branding properties can be found [here](http://docs.frevvo.com/d/display/frevvo74/Installation+Customizations).

## UI CSS Changes

This starter repository includes a `webapps/ROOT/frevvo-style.css` where specific UI customizations can be implemented using CSS rules. This CSS file is hooked up by the `frevvo.css.url` property found in the `frevvo-config.properties`.

## Automatic Tenant Creation

The sample Liquibase `db_changelog.xml` will automatically create an `OEM` tenant when frevvo runs on a fresh database.

There are a few things to note here:
* It creates an `OEM` tenant using the `DelegatingSecurityManager` [Security Manager](http://docs.frevvo.com/d/display/frevvo74/Security+Managers). You may want to change that depending on your integration requirements.
* It creates an `admin` tenant admin on this new tenant with UI login disabled: the assumption is that the login will be performed by the API only.
* It disables logins for the global user `admin@d` so the only way to access frevvo is through the API.

If you need to change any of these defaults, you will need to modify these files.
